
resource "aws_s3_bucket" "lambda_bucket" {
  bucket        = "labda-deployment-code-bucket-for-${var.LAMBDA_FUNCTION_NAME}"
  force_destroy = true
}

resource "aws_s3_bucket_public_access_block" "lambda_bucket" {
  bucket = aws_s3_bucket.lambda_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}