# node-backend-using-aws-terraform

## Getting started

infrastructure folder contains the terraform files to create server API Gateway, Lambda, IAM role etc

# Configure aws credential

1. run following command to configure aws user cred locally
   aws configure --profile <profile-name>
2. Set default profile
   export AWS_PROFILE=<profile-name>
3. List the profiles
   aws configure list-profiles
4. Environment variable should have `TF_VAR_` prefix
